package ru.kuzin.tm.exception.field;

public class ExistsLoginException extends AbstractFieldException {

    public ExistsLoginException() {
        super("Error! Login already exists...");
    }

}
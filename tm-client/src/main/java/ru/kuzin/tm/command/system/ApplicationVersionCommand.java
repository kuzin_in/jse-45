package ru.kuzin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.dto.request.ApplicationVersionRequest;
import ru.kuzin.tm.dto.response.ApplicationVersionResponse;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "version";

    @NotNull
    private static final String DESCRIPTION = "Show version info.";

    @NotNull
    private static final String ARGUMENT = "-v";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        @NotNull ApplicationVersionRequest request = new ApplicationVersionRequest();
        @NotNull ApplicationVersionResponse response = getSystemEndpoint().getVersion(request);
        System.out.println(response.getVersion());
    }

}
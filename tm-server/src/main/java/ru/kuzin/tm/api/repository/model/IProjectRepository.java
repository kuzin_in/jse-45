package ru.kuzin.tm.api.repository.model;

import ru.kuzin.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {
}
